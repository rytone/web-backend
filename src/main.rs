use std::error::Error;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader};
use std::net::SocketAddr;
use std::path::Path;
use std::sync::Arc;

use futures::Future;
use handlebars::{Handlebars, TemplateFileError};
use rand::seq::SliceRandom;
use serde::{Deserialize, Serialize};
use structopt::StructOpt;
use warp::{path, Filter};

mod aur;
mod recover;

// load paths
const WEB_PATH: &str = "/home/max/web";
const AUR_PATH: &str = "/home/aur/aur";

fn load_handlebars() -> Result<Handlebars, TemplateFileError> {
    let mut hb = Handlebars::new();

    hb.set_strict_mode(true);
    hb.register_templates_directory(".hbs", Path::new(WEB_PATH).join("handlebars"))?;

    Ok(hb)
}

fn load_phrases() -> Result<Vec<String>, io::Error> {
    let file = File::open(Path::new(WEB_PATH).join("phrases.txt"))?;
    let buf = BufReader::new(file);
    Ok(buf.lines().collect::<Result<_, io::Error>>()?)
}

fn render_home(
    phrase: Option<String>,
    reset: bool,
    hb: Arc<Handlebars>,
    phrases: Arc<Vec<String>>,
) -> impl warp::Reply {
    #[derive(Serialize)]
    struct Data {
        phrase: String,
    };

    let phrase = if reset {
        phrases
            .as_slice()
            .choose(&mut rand::thread_rng())
            .unwrap()
            .clone()
    } else {
        phrase.unwrap_or_else(|| {
            phrases
                .as_slice()
                .choose(&mut rand::thread_rng())
                .unwrap()
                .clone()
        })
    };

    let data = Data {
        phrase: phrase.clone(),
    };
    let body = hb
        .render("index", &data)
        .unwrap_or_else(|e| e.description().to_owned());

    let mut response = warp::http::Response::builder();
    response.header(
        "Set-Cookie",
        format!("phrase={}; Expires=Mon, 21 Jun 2100 00:00:00 GMT", phrase),
    );

    if reset {
        response.status(307);
        response.header("Location", "/");
    }

    response.body(body)
}

#[derive(StructOpt)]
struct Opt {
    #[structopt(long = "addr")]
    addr: Option<SocketAddr>,
    #[structopt(long = "aur-addr")]
    aur_addr: Option<SocketAddr>,
}

fn main() {
    pretty_env_logger::init();
    let opt = Opt::from_args();

    let hb = Arc::new(load_handlebars().expect("failed to load handlebars templates"));
    let phrases = Arc::new(load_phrases().expect("failed to load phrases"));

    #[derive(Deserialize)]
    struct HomeQuery {
        reset: Option<String>,
    };
    let home_hb = Arc::clone(&hb);
    let home = warp::get2()
        .and(warp::path::end())
        .and(warp::cookie::optional("phrase"))
        .and(warp::query::<HomeQuery>())
        .map(|phrase, query: HomeQuery| (phrase, query.reset.is_some()))
        .map(move |(phrase, reset)| {
            render_home(phrase, reset, Arc::clone(&home_hb), Arc::clone(&phrases))
        });

    let static_serve = warp::fs::dir(Path::new(WEB_PATH).join("static"));

    let cors = warp::cors()
        .allow_origin("https://ryt.one")
        .allow_origin("https://aur.ryt.one");

    let asset_serve = path!("assets")
        .and(warp::fs::dir(Path::new(WEB_PATH).join("assets")));

    let aur_api = path!("list" / String).and_then(aur::api_list_packages);

    let browse_hb = Arc::clone(&hb);
    let aur_browse = path!("browse" / String)
        .and_then(move |repo| aur::browse_repo(repo, Arc::clone(&browse_hb)));

    let aur_browse_js =
        warp::path("ui.js").and(warp::fs::file(Path::new(WEB_PATH).join("aur-ui/ui.js")));

    let aur = warp::get2()
        .and(aur_browse_js.or(aur_browse).or(aur_api))
        .or(warp::fs::dir(AUR_PATH));

    let main_recover_hb = Arc::clone(&hb);
    let main = home
        .or(static_serve)
        .or(asset_serve)
        .recover(move |e| recover::recover(Arc::clone(&main_recover_hb), e))
        .with(cors);

    let aur_recover_hb = Arc::clone(&hb);
    let aur = aur.recover(move |e| recover::recover(Arc::clone(&aur_recover_hb), e));

    let bind_addr = opt
        .addr
        .unwrap_or_else(|| SocketAddr::from(([127, 0, 0, 1], 8080)));
    let aur_bind_addr = opt
        .aur_addr
        .unwrap_or_else(|| SocketAddr::from(([127, 0, 0, 1], 8081)));

    let (_, main_server) = warp::serve(main).bind_ephemeral(bind_addr);
    let (_, aur_server) = warp::serve(aur).bind_ephemeral(aur_bind_addr);

    let total_server = main_server.join(aur_server);
    tokio::run(total_server.map(|_| ()));
}
