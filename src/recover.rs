use std::sync::Arc;

use handlebars::Handlebars;
use serde::Serialize;
use warp;

pub fn recover(hb: Arc<Handlebars>, err: warp::Rejection) -> Result<impl warp::Reply, warp::Rejection> {
    #[derive(Serialize)]
    struct Data { code: String };

    let data = Data { code: err.status().as_str().into() };

    let body = hb.render("error", &data).map_err(|e| warp::reject::custom(e))?;

    Ok(warp::reply::with_status(warp::http::Response::new(body), err.status()))
}
