use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;
use std::sync::Arc;
use std::time::SystemTime;

use futures::future::{Future, poll_fn};
use handlebars::Handlebars;
use lazy_static::lazy_static;
use regex::Regex;
use serde::Serialize;
use serde_json;
use tar::{Archive, EntryType};
use tokio_threadpool;
use warp;

lazy_static! {
    static ref DATABASES: HashMap<&'static str, &'static str> = {
        let mut m = HashMap::new();
        m.insert("aur", "repos/stable/aur.db");
        m.insert("aur-devel", "repos/devel/aur-devel.db");
        m
    };
}

#[derive(Serialize)]
struct Package {
    #[serde(rename="n")]
    pub name: String,
    #[serde(rename="v")]
    pub version: String,
    #[serde(rename="d")]
    pub description: String,
}

#[derive(Serialize)]
struct Repo {
    #[serde(rename="r")]
    pub name: String,
    #[serde(rename="p")]
    pub packages: Vec<Package>,
    #[serde(rename="u")]
    pub last_updated : SystemTime,
}

fn get_repo<T: AsRef<str>>(repo: T) -> Result<Repo, io::Error> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?:.*\n)*?(?:%NAME%\n)(.*)\n(?:.*\n)*?(?:%VERSION%\n)(.*)\n(?:.*\n)*?(?:%DESC%\n)(.*)").unwrap();
    }

    let repo_path = if let Some(p) = DATABASES.get(repo.as_ref()) {
        p
    } else {
        return Err(io::Error::new(io::ErrorKind::NotFound, "repo does not exist!"));
    };

    let db_file = File::open(Path::new(crate::AUR_PATH).join(repo_path))?;
    let last_updated = db_file.metadata()?.modified()?;
    let mut db_archive = Archive::new(db_file);

    let mut packages: Vec<Package> = Vec::new();
    for file in db_archive.entries()? {
        let mut file = file?;

        if file.header().entry_type() != EntryType::Regular {
            continue;
        }

        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        let data = RE.captures_iter(&contents)
            .map(|cap| Package {
                name: String::from(&cap[1]),
                version: String::from(&cap[2]),
                description: String::from(&cap[3]),
            });
        packages.extend(data);
    }

    Ok(Repo {
        name: String::from(repo.as_ref()),
        packages,
        last_updated,
    })
}

fn get_packages_async(repo: String) -> impl Future<Item = Repo, Error = io::Error> {
    poll_fn(move || tokio_threadpool::blocking(|| get_repo(&repo)))
        .then(|res| match res {
            Ok(r) => r,
            Err(_) => Err(io::Error::new(io::ErrorKind::Other, "blocking fail"))
        })
}

pub fn api_list_packages(repo: String) -> impl Future<Item = String, Error = warp::Rejection> {
    get_packages_async(repo)
        .map_err(|e| match e.kind() {
            io::ErrorKind::NotFound => warp::reject::not_found(),
            _ => warp::reject::custom(e),
        })
        .and_then(|repo| serde_json::to_string(&repo).map_err(|e| warp::reject::custom(e)))
}

pub fn browse_repo(repo: String, hb: Arc<Handlebars>) -> impl Future<Item = warp::http::Response<String>, Error = warp::Rejection> {
    api_list_packages(repo)
        .and_then(move |serialized| {
            #[derive(Serialize)]
            struct Data {
                flags: String,
            }

            let data = Data { flags: serialized };

            let body = hb.render("aur-ui", &data)
                .map_err(|e| warp::reject::custom(e))?;

            Ok(warp::http::Response::new(body))
        })
}
